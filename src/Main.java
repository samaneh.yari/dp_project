import com.sun.scenario.effect.impl.sw.sse.SSEBlend_SRC_OUTPeer;

import java.util.ArrayList;
import java.util.Scanner;
import static jdk.nashorn.internal.objects.Global.println;
import static jdk.nashorn.internal.runtime.ScriptObject.setGlobalObjectProto;
public class Main {
    public static void main(String[] args) {

        ArrayList<ArrayList<String>> inputs = new ArrayList<ArrayList<String>>();
        ArrayList<ArrayList<Integer>> relocateCost = new ArrayList<ArrayList<Integer>>();
        Scanner scanner = new Scanner(System.in);
        System.out.println("Enter number of cities :");
        int k = scanner.nextInt();
        System.out.println("Enter number of months :");
        int n = scanner.nextInt();
        String[] path = new String[n];
        for (int i = 0; i < k; i++) {
            inputs.add(new ArrayList<String>());
        }
        for (int i = 0; i < k ; i++) {
            relocateCost.add(new ArrayList<Integer>());
        }
        System.out.println("Enter name of cities :");
        for (int i = 0; i < k; i++) {
            for (int j = 0; j <= n; j++) {
                inputs.get(i).add(j, scanner.next());
            }
        }
        System.out.println("Enter Fij :");
        for (int i = 0; i < k ; i++) {
            for (int j = 0; j < k ; j++) {
                if (i != j) {
                    System.out.println("F" + i + j + " : ");
                    relocateCost.get(i).add(j, scanner.nextInt());
                } else {
                    relocateCost.get(i).add(j, 0);
                }
            }
        }
        dpSolution(fillMatrix(inputs,k), relocateCost , inputs, k, n);
        //System.out.println("*******" + dpSolution(fillMatrix(inputs,k), relocateCost , inputs, k, n));

    }
    public static ArrayList<ArrayList<Integer>> fillMatrix(ArrayList<ArrayList<String>> input, int k) {
        ArrayList<ArrayList<Integer>> dpMatrix = new ArrayList<ArrayList<Integer>>();
        for (int i = 0; i < k; i++) {
            dpMatrix.add(new ArrayList<Integer>());
        }
        for (int i = 0; i < k; i++) {
            dpMatrix.get(i).add(0, Integer.valueOf(input.get(i).get(1)));
        }
        return dpMatrix;
    }


    public static void dpSolution(ArrayList<ArrayList<Integer>> dpMatrix, ArrayList<ArrayList<Integer>> relocateCost,
                                 ArrayList<ArrayList<String>> inputs, int k, int n){
        dpMatrix = fillMatrix(inputs, k);
        String[] path = new String[n];
        for (int j = 1; j < n; j++) {
            for (int i = 0; i < k; i++) {
                dpMatrix.get(i).add(j, (Integer.valueOf(inputs.get(i).get(j+1)) + optimalChoice(i, j, dpMatrix, relocateCost, k).get(0)));

            }
        }

        int result = dpMatrix.get(k - 1).get(n - 1);
        int a = k - 1;
        for (int p = 0 ; p < k ; p++){
            if (dpMatrix.get(p).get(n-1) < result){
                result = dpMatrix.get(p).get(n-1);
                a = p;
            }
        }
        System.out.println("minimum cost :" + result);

        path[0] = inputs.get(a).get(0);
        int j = n-1;
        for (int i = 1; i <n ; i++) {
            path[i] = inputs.get(optimalChoice(a,j,dpMatrix,relocateCost,k).get(1)).get(0);
            a = optimalChoice(a,j,dpMatrix,relocateCost,k).get(1);
            j--;
        }
        System.out.printf("Path : ");
        for (int i = n-1; i >= 0 ; i--) {
            System.out.printf(path[i] + " ");

        }

    }


    public  static ArrayList<Integer> optimalChoice(int i , int j, ArrayList<ArrayList<Integer>> dpMatrix, ArrayList<ArrayList<Integer>> relocateCost , int k ){
        ArrayList<Integer> out = new ArrayList<>();
        int [] arr = new int[k];
        int z = 0;
        int x = 0;
        int min = dpMatrix.get(z).get(j-1)+ relocateCost.get(z).get(i);
        for ( z = 0; z < k; z++){
            arr[z] = dpMatrix.get(z).get(j-1)+ relocateCost.get(z).get(i);
            if (arr[z] < min) {
                min = arr[z];
                x = z;
            }
        }
        out.add(min);
        out.add(x);

        return out;
    }
}

